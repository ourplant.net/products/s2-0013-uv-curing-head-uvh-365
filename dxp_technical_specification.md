The S2-0013 UV-CURING HEAD UVH-365 has the following technical specifications.

## Technical information
|Parameter|Value|
|:----|----:|
|Abmessungen in mm (B x T x H)|49 x 224 x 514|
|Gewicht in kg|4|
|Verfahrbereich in Z in mm|150|
|Spannung in V|24|
|Max. Stromstärke in A|4|
|Kommunikationsschnittstelle|UNICAN|

