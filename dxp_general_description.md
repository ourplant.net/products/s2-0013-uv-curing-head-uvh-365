The UV Curing Head is equipped with a UV LED spot to cure and pre-activate photoactive adhesives. This module is available with different lenses.

| **Lens** | **max. intensity** **(mW/cm²)** | **Working distance (mm)** | **Spot diameter (mm)** |
| -------- | ------------------------------- | ------------------------- | ---------------------- |
| A        | 12000                           | 3                         | 0,6                    |
| B        | 5000                            | 6                         | 1                      |
| C        | 2000                            | 8                         | 2                      |
| D        | 1100                            | 10                        | 3                      |

The intensity of the lens is individually adjustable by software. Lens B is included in the scope of delivery. The lenses A,C, and D can be ordered optionally.