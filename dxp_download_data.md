Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0013-uv-curing-head-uvh-365).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0013-uv-curing-head-uvh-365/-/raw/main/01_operating_manual/S2-0013_A1_Betriebsanleitung.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0013-uv-curing-head-uvh-365/-/raw/main/09_assembly_drawing/s2-0013_A_ZNB_uv-curing_head_uvh-365.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0013-uv-curing-head-uvh-365/-/raw/main/03_circuit_drawing/S2-0013-EPLAN-A.pdf)|
|maintenance instructions   ||
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0013-uv-curing-head-uvh-365/-/raw/main/02_spare_parts/S2-0013_C_EVL.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
